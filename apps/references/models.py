#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-
from StringIO import StringIO
import uuid,urllib,urllib2
import os
import json

try:
    from bibtexparser.bibtexparser import BibTexParser
except:
    pass

from django.contrib.auth.models import AbstractUser

from django.core.urlresolvers import reverse


from django.views.decorators.csrf import csrf_exempt


import wioframework.fields as models
from wioframework import gen2


from wioframework import decorators as dec
from wioframework import validators
from wioframework.amodels import wideiomodel, wideio_owned, wideio_timestamped_autoexpiring, get_dependent_models, wideio_timestamped, wideio_action, wideio_publishable
from extfields.fields import ResizedImageField, PyrImageField
from django.core.files.base import ContentFile


from extfields.imageupload import fields

try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO
            
import settings
MODELS = []

@wideio_publishable()
@wideio_owned()
@wideio_timestamped_autoexpiring()
@wideiomodel
class Image(models.Model):
    name = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        db_validators=[
            validators.NameValidator(
                with_underscore=True)])
    # optional_source_url=models.URLField(null=True,blank=True) #< optional
    # source url
    image = PyrImageField(upload_to="upload/imgs/webpics",
                              max_length=255, blank=True, null=True
                              )
    reference = models.ForeignKey(
        'Image',
        related_name="original_image",
        null=True,
        blank=True)
    is_public = models.BooleanField(default=False)
    #tags= models.ManyToManyField(keywords)
    orig_url = models.URLField(blank=True, null=True, db_index=True)

    @staticmethod
    def _gim(query, results=4):
          from apiclient.discovery import build        
          service = build("customsearch", "v1", developerKey= settings.get("GOOGLE_API_KEY"))
          res = service.cse().list(
                q=query,
#                cx=' ** your cx **',  # custom search
                searchType='image',
                num=4,
                fileType='png',
                safe= 'on'
          ).execute()

          return urls
                                        
      # ni.set_expire(expire=datetime.timedelta(minutes=10)) # make it expire

         
    @classmethod
    def invoke_by_url(cls,url,force=False):
        qs=Image.objects.filter(orig_url=url)
        if force:
            if (qs.count()):            
               qs.delete()
            qs=Image.objects.filter(orig_url=url)            
        if (qs.count()):
              return qs[0]
        else:
              ni=Image()
              ni.orig_url = url
              ni.wiostate='V'
              ni.save()        
              from urlparse import urlparse
              from os.path import splitext, basename
              disassembled = urlparse(url)
              filename, file_ext = splitext(basename(disassembled.path))
              new_content = StringIO()
              new_content.write(urllib.urlopen(url).read())
              new_content = ContentFile(new_content.getvalue())
              # OF SERVER THREADS
              image_name = filename + file_ext
              fields.PyrImageField.pyr_save(ni.image,image_name,new_content)
              ni.save()
              return ni
              
    @classmethod
    def gim(cls,query,force=False):
        for i in range(4):
            try:
              url=cls._gim(query)[i]
              return cls.invoke_by_url(url,force)
            except:
               pass
              
              
    def get_scales(self):
        return [512, 256, 128, 64]

    def get_all_references(self):
        return []

    def is_broken(self):
        x=self.for_scale(512,512)
        return not os.path.exists(os.path.join("media",x))
    
    @staticmethod
    def remove_all_broken_images():
      for i in Image.objects.all():
        if i.is_broken():
         i.delete()
    
    def __unicode__(self):
        #return 'webdata/' + unicode(self.image)
        pg1=PyrImageField.pyr_get(self.image)
        if pg1 is None:
            return u"image_not_found"
        return u'webdata/' + pg1

    def for_scale(self,w=128,h=128):
        #return 'webdata/' + unicode(self.image)
        pg1=PyrImageField.pyr_get(self.image,w,h)
        if pg1 is None:
            return None        
        return 'webdata/' + pg1


    def on_add(self, request):
        self.update_from_request_files(request)

    def on_update(self, request):
        self.update_from_request_files(request)

    def update_from_request_files(self, request):
        if self.image and "image" in request.FILES:
            f = request.FILES["image"]
            f.seek(0)
            # not sure if this is needed as seems to be saved on other levels
            PyrImageField.pyr_save(self.image,"imgupf-" + self.id, ContentFile(f.read()))
            f.seek(0)
        return

    def on_delete(self, request):
        PyrImageField.pyr_delete(self.image)
        
    def do_save(self, name,f):
            f.seek(0)
            # not sure if this is needed as seems to be saved on other levels
            #self.name="imgupf-" + self.id,
            PyrImageField.pyr_save(self.image,"imgupf-" + self.id, (f if isinstance(f,ContentFile) else ContentFile(f.read())))
            f.seek(0)
            self.save()

    def on_view(self, request):
        try:
          if self!=None:
            return {
              'name': self.name,
              'image_path': unicode(self)
            }
        except Exception,e:
          print "Image OnView",e
          pass
        

    class WIDEIO_Meta:
        permissions = dec.perm_for_logged_users_only
        form_exclude = ['reference']
        allow_query = ['is_public','name']
        # API_EXPORT_EXCLUDE=["image"]
        audlv_xargs = {'update_decorators': [csrf_exempt],'add_decorators': [csrf_exempt]}

        class Actions:
            @wideio_action(
                "make_public", possible=lambda s, r: (r.user.is_staff and not s.is_public))
            def make_public(self, request):
                self.is_public = True
                self.save()
            @wideio_action(
                "make_private", possible=lambda s, r: (r.user.is_staff and s.is_public))
            def make_private(self, request):
                self.is_public = False
                self.save()                

MODELS.append(Image)
MODELS += get_dependent_models(Image)


@wideio_publishable()
@wideiomodel
@wideio_timestamped
class Link(models.Model):

    """
    Website as a reference.

    These links are tracked and thus are not meant to be modified.
    They are elements on which we perform analytics.
    """
    target = models.URLField()
    click_count = models.IntegerField(default=0, editable=False)
    # click_count_history = ListField(models.IntegerField(), default=lambda:
    # []) # month
    use_count = models.IntegerField(default=0, editable=False)
    #use_count_history = ListField(models.IntegerField(), default=lambda: [])

    def __unicode__(self):
        return self.target

    def get_all_references(self):
        return []

    class WIDEIO_Meta:
        sort_enabled = ['target', 'use_count', 'click_count']
        search_enabled = 'target'
        permissions = dec.all_permissions_from_request_rwa(
            lambda r: True,
            lambda r: r.user.is_superuser,
            lambda r: (
                r.user is not None))

        class Actions:

            @wideio_action()
            def follow(self, request):
                from django.http import HttpResponseRedirect
                return HttpResponseRedirect(self.target)

MODELS.append(Link)
MODELS += get_dependent_models(Link)


@wideio_publishable()
@wideio_timestamped
@wideio_owned()
@wideiomodel
class GenericReference(models.Model):

    """
     Reference to a link on a page owned by the owner on a page
    """
    title = models.CharField(
        max_length=128,
        db_validators=[
            validators.NameValidator()])
    #gpk_type = models.ForeignKey(ContentType, null=True, related_name="content_type_set_for_%(class)s")
    gpk_pk = models.TextField('object ID', null=True)
    link = gen2.GenericForeignKey2(fk_field="gpk_pk")

    def get_all_references(self, n=None):
        if self.link_id:
            return [self.link]
        return []

    class WIDEIO_Meta:
        pass

    def __unicode__(self):
        return self.title + unicode(self.link)


MODELS.append(GenericReference)
MODELS += get_dependent_models(GenericReference)


@wideio_publishable()
@wideio_timestamped
@wideio_owned()
@wideiomodel
class LinkReference(models.Model):

    """
    Reference to a link on a page owned by the owner on a page
    """
    title = models.CharField(
        max_length=128,
        db_validators=[
            validators.NameValidator(
                with_underscore=True)])
    link = models.ForeignKey(Link)

    class WIDEIO_Meta:
        pass

    def __unicode__(self):
        return self.title + unicode(self.link)

    def get_all_references(self):
        return []

    @classmethod
    def create(cls, title, url):
        l = Link.objects.filter(target=url)
        if l.count() == 0:
            l = Link(target=url)
            l.save()
        else:
            l = l[0]
        lr = LinkReference(title=title, link=l)
        lr.save()
        return lr

    class WIDEIO_Meta:
        search_enabled = "id"

MODELS.append(LinkReference)
MODELS += get_dependent_models(LinkReference)


@wideio_publishable()
@wideio_timestamped
@wideio_owned()
@wideiomodel
class BibliographicReference(models.Model):

    """
    BIBTEX / OTHER APIS ?
    """
    title = models.CharField(max_length=255)
    # doi=models.CharField(max_length=255)
    # all fields the potentially interesting fields will be extracted from the
    # bibtex
    bibtex = models.TextField(max_length=6000)
    use_count = models.IntegerField(default=0, editable=False)
    #use_count_history = ListField(models.IntegerField(), default=lambda: [])

    # gpk_type   = models.ForeignKey(ContentType, null=True,
    # related_name="content_type_set_for_%(class)s")
    #gpk_pk = models.TextField('object ID', null=True)
    #generic_pk = generic.GenericForeignKey(ct_field="gpk_type", fk_field="gpk_pk")
    def get_all_references(self):
        return []

    def __unicode__(self):
        return self.title

    def get_doi(self):
        b = BibTexParser(StringIO(unicode(self.bibtex)))
        return b.get_entry_list()[0][u'doi']

    def get_author_names(self):
        b = BibTexParser(StringIO(unicode(self.bibtex)))
        return b.get_entry_list()[0][u'author']

    def get_title(self):
        b = BibTexParser(StringIO(unicode(self.bibtex)))
        return b.get_entry_list()[0][u'title']

 # t="""@Book{hicks2001,
 #author    = "von Hicks, III, Michael",
 # title     = "Design of a Carbon Fiber Composite Grid Structure for the GLAST
        # Spacecraft Using a Novel Manufacturing Technique",
 #publisher = "Stanford Press",
 #year      =  2001,
 #address   = "Palo Alto",
 #edition   = "1st",
 #isbn      = "0-69-697269-4"
 #}"""

    def __unicode__(self):
        return self.title

    class WIDEIO_Meta:
        sort_enabled = ['title', 'use_count']
        search_enabled = 'title'

MODELS.append(BibliographicReference)
MODELS += get_dependent_models(BibliographicReference)
