#!/bin/bash

ST=""
if git stash; then
  ST="1"
fi
git flow init -d
[ -d $PWD/.git/hooks ] && rm -r $PWD/.git/hooks
rm -f $PWD/.git/hooks
ln -s $PWD/.git_hooks $PWD/.git/hooks
if [ "$ST" ]; then
git stash pop
fi
